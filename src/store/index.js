import Vue from 'vue';
import Vuex from 'vuex';
import tb from 'to-boolean';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        debug: tb(process.env.VUE_APP_STAGING),
        wind: false,
        enableBrokenRoutes: false,
        client: {
            clientVersion: process.env.VUE_APP_VERSION,
            latestClientVersion: '',
            latestAPIVersion: '',
            bannerText: '',
            bannerEnabled: false,
            bannerId: 0,
            registrationsDisabled: false,
            name: "Kaverti",
            logo: '',
            icon: '',
            release: process.env.VUE_APP_RELEASE,
            development: window.location.hostname === 'dev.kaverti.flowinity' || window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1',
            secure: window.location.protocol === "https:",
            domain: window.location.hostname,
            buildDate: process.env.VUE_APP_BUILD_DATE,
            lang: 'debug'
        },
        errors: {
            errors: null,
            modal: false
        },
        banned: {
            banned: false,
            message: "Not banned",
            readOnlyMode: false,
            ipBanned: false,
            disableLogin: false,
            expiry: "2020-01-01T00:00:00.000Z"
        },
        user: {
            id: 0,
            username: '',
            email: '',
            admin: false,
            avatar: 'default',
            bot: '',
            system: '',
            token: '',
            koins: 0,
            emailVerified: false,
            modeler: false,
            developerMode: false,
            executive: false,
            description: '',
            conversations: [],
            currentConversation: 0
        }
    },
    mutations: {
        setSettings(state, value) {
            state.client.name = value.siteName
            state.client.bannerEnabled = value.bannerEnabled
            state.client.bannerText = value.bannerText
            state.client.registrationsDisabled = value.RegistrationsDisabled
            state.client.logo = value.logo
            state.client.icon = value.icon
        },
        setVersion(state, value) {
            state.client.latestClientVersion = value
        },
        setAPIVersion(state, value) {
            state.client.latestAPIVersion = value
        },
        turnOffDebug(state) {
            state.debug = false
        },
        fakeUser(state) {
            state.user.id = 1
            state.user.username = "Debug"
            state.user.email = "valid@troplo.com"
            state.user.admin = false
            state.user.avatar = 'default'
            state.user.bot = false
            state.user.system = false
            state.user.token = "1234"
            state.user.koins = 69
            state.user.emailVerified = true
        },
        setAjaxErrorsModalState(state, value) {
            state.errors.modal = value;
        },
        setAjaxErrors(state, value) {
            state.errors.errors = value
        },
        setUsername(state, value) {
            state.user.username = value
        },
        setToken(state, value) {
            state.user.token = value
        },
        setBanned(state, value) {
            state.banned.message = value
            state.banned.banned = value
            state.banned.readOnlyMode = value
            state.banned.disableLogin = value
            state.banned.ipBanned = value
            state.banned.expiry = value
        },
        setAvatar(state, value) {
            state.user.avatar = value
        },
        setBot(state, value) {
            state.user.bot = value
        },
        setEmail(state, value) {
            state.user.email = value
        },
        setEmailVerified(state, value) {
            state.user.emailVerified = value
        },
        setID(state, value) {
            state.user.id = value
        },
        setWind(state, value) {
            state.wind = value
        },
        setKoins(state, value) {
            state.user.koins = value
        },
        setAdmin(state, value) {
            state.user.admin = value
        },
        setExecutive(state, value) {
            state.user.executive = value
        },
        setDescription(state, value) {
            state.user.description = value
        },
        brokenRoute(state, value) {
            state.enableBrokenRoutes = value
        },
        clearConversations(state) {
            state.user.conversations = [];
        },
        addConversations(state, conversations) {
            state.user.conversations.push(...conversations);
        },
        updateConversationLastRead(state, id) {
            let index = state.user.conversations.findIndex(conversation => {
                return conversation.id === id;
            });

            let conversation = state.user.conversations[index];
            conversation.lastRead = new Date() + '';

            state.user.conversations.splice(index, 1, conversation);
        },
        updateUnshiftConversation(state, updatedConversation) {
            let index = state.user.conversations.findIndex(conversation => {
                return conversation.id === updatedConversation.id;
            });

            if (index > -1) {
                state.user.conversations.splice(index, 1);
            }

            state.user.conversations.unshift(updatedConversation);
        },
        updateConversationName(state, { id, name }) {
            let index = state.user.conversations.findIndex(conversation => {
                return conversation.id === id;
            });

            let conversation = state.user.conversations[index];
            conversation.name = name;

            state.user.conversations.splice(index, 1, conversation);
        },
        setCurrentConversation(state, id) {
            state.user.currentConversation = id
        },
        setSecure(state, value) {
            state.client.secure = value;
        },
        issueLogout(state) {
            state.user.username = ''
            state.user.token = ''
            state.user.koins = 0
            state.user.id = 0
        },
        setLang(state, value) {
            state.client.lang = value
        }
    },
    actions: {},
    modules: {}
});