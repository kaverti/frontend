import Vue from 'vue';
import VueRouter from 'vue-router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
Vue.use(VueRouter);
NProgress.configure({ showSpinner: false });

function route(view) {
    return () =>
        import (`@/views/${view}.vue`)
}

const routes = [{
        path: '/',
        name: 'Home',
        component: route('Home')
    },
    {
        path: '/about',
        name: 'About',
        component: route('About')
    },
    {
        path: '/debug',
        name: 'Debug',
        component: route('Debug')
    },
    {
        path: '/u/:username',
        redirect: '/u/:username/awards',
        component: route('User'),
        children: [
            { path: 'posts', component: route('UserPosts') },
            { path: 'threads', component: route('UserThreads') },
            { path: 'items', component: route('UserMarketplace') },
            { path: 'wall', component: route('UserWall') },
            { path: 'inventory', component: route('UserInventory') },
            { path: 'friends', component: route('UserFriends') },
            { path: 'awards', component: route('UserAwards') }
        ]
    },
    {
        path: '/users',
        name: 'Users',
        component: route('Users')
    },
    {
        path: '/teams',
        name: 'Teams',
        component: route('Teams')
    },
    {
        path: '/character',
        redirect: '/avatar',
        name: 'Character',
        component: route('Avatar')
    },
    {
        path: '/avatar',
        redirect: '/avatar/hats',
        name: 'Avatar',
        component: route('Avatar')
    },
    {
        path: '/avatar/:category',
        name: 'Avatar',
        component: route('Avatar')
    },
    {
        path: '/register',
        name: 'Register',
        component: route('Register')
    },
    {
        path: '/login',
        name: 'Login',
        component: route('Login')
    },
    {
        path: '/t/:username',
        component: route('Team'),
        children: [
            { path: 'posts', component: route('UserPosts') },
            { path: 'threads', component: route('UserThreads') },
            { path: 'items', component: route('UserMarketplace') },
            { path: 'wall', component: route('UserWall') },
            { path: 'inventory', component: route('UserInventory') },
            { path: 'friends', component: route('UserFriends') },
            { path: 'awards', component: route('UserAwards') }
        ]
    },
    {
        path: '/chat',
        component: route('Chat'),
        redirect: ('/chat/home'),
        children: [
            { path: '/', component: route('ChatHome') },
            { path: '/home', component: route('ChatHome') },
            { path: 'conversation', component: route('ChatMessage') },
            { path: 'conversation/:id', component: route('ChatMessage'), name: 'conversation' }
        ]
    },
    {
        path: '/awards',
        name: 'Awards',
        component: route('Awards')
    },
    {
        path: '/transactions',
        name: 'Transactions',
        component: route('Transaction')
    },
    {
        path: '/forums',
        redirect: '/forums/ALL',
        name: 'Forums',
        component: route('Forums')
    },
    {
        path: '/forums/create',
        name: 'ForumThreadCreate',
        component: route('ForumThreadCreate')
    },
    {
        path: '/forums/:category',
        name: 'Forums',
        component: route('Forums')
    },
    {
        path: '/forums/thread/:id',
        name: 'ForumThread',
        component: route('ForumThread')
    },
    {
        path: '/roadmap',
        name: 'Roadmap',
        component: route('Roadmap')
    },
    {
        path: '/marketplace/',
        redirect: '/marketplace/HATS',
        name: 'Marketplace',
        component: route('Marketplace')
    },
    {
        path: '/marketplace/:category',
        name: 'Marketplace',
        component: route('Marketplace')
    },
    {
        path: '/m/:id',
        name: 'MarketplaceItem',
        component: route('MarketplaceItem')
    },
    {
        path: '/friends',
        redirect: '/friends/accepted',
        name: 'Friends',
        component: route('Friends')
    },
    {
        path: '/friends/:category',
        name: 'Friends',
        component: route('Friends')
    },
    {
        path: '/stats',
        name: 'Stats',
        component: route('Stats')
    },
    {
        path: '/admin',
        component: route('Admin'),
        redirect: "/admin/dashboard",
        children: [
            { path: 'dashboard', component: route('AdminDashboard') },
            { path: 'item', component: route('AdminCreateItem') },
        ]
    },
    {
        path: '/settings',
        component: route('Settings'),
        redirect: "/settings/general",
        children: [
            { path: 'general', component: route('SettingsGeneral') },
            { path: 'security', component: route('SettingsSecurity') },
            { path: 'about', component: route('SettingsAbout') },
        ]
    },
    {
        path: '/verify/:code',
        name: 'EmailVerify',
        component: route('EmailVerify')
    },
    {
        path: '*',
        name: '404',
        component: route('404')
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start();
    }
    next();
})

router.afterEach(() => {
    NProgress.done();
})

export default router;