import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Buefy, { Snackbar } from "buefy";
import axios from "axios";
import VueAxios from "vue-axios";
import i18n from "./i18n";
import moment from "moment";
import VMdEditor from "@kangc/v-md-editor";
import "@kangc/v-md-editor/lib/style/base-editor.css";
import enUS from "@kangc/v-md-editor/lib/lang/en-US";
import createHljsTheme from "@kangc/v-md-editor/lib/theme/hljs";
import json from "highlight.js/lib/languages/json";
const hljsTheme = createHljsTheme();
import io from "socket.io-client";
import VueSocketIO from "vue-socket.io";
import NProgress from "vue-nprogress";

Vue.use(NProgress);
const nprogress = new NProgress();
Vue.use(
    new VueSocketIO({
        debug: true,
        connection: io(process.env.VUE_APP_GATEWAY_ENDPOINT), // options object is Optional
    })
);
Vue.use(VMdEditor);
Vue.use(VueAxios, axios);
Vue.use(Buefy);

Vue.config.productionTip = false;
Vue.prototype.$snackbar = Snackbar;

axios.defaults.headers.common["Authorization"] = localStorage.getItem("token")

// BBCode Configuration
VMdEditor.lang.use("en-US", enUS);
hljsTheme.extend((md, hljs) => {
    md.set({
        html: false,
        xhtmlOut: false,
        breaks: true,
        langPrefix: "language-",
        linkify: true,
        image: false,
        typographer: true,
        quotes: "“”‘’"
    });
    md.disable("image");
    hljs.registerLanguage("json", json);
});

// Date Format Configuration
Vue.filter("formatDate", function(value) {
    if (value) {
        return moment(String(value)).format("hh:mm A, DD/MM/YYYY");
    }
});

new Vue({
    nprogress,
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount("#app");