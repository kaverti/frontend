import { SnackbarProgrammatic as Snackbar } from 'buefy'
export default function (vuex) {
	return function (res, ignorePathErrorCb) {
		let errors = []

		if(res.response === undefined || res.response.data.errors === undefined) {
			errors.push('Kaverti ran into an unknown issue while attempting to perform this action, try retrying it, or try it later.')
		} else {
			res.response.data.errors.forEach(error => {
				let path = error.path

				if(path && ignorePathErrorCb) {
					ignorePathErrorCb(error, errors)
					return
				}
				errors.push(error.message[0].toUpperCase() + error.message.slice(1))
			})
		}
		if(errors.length) {
			vuex.commit('setAjaxErrors', errors)
			Snackbar.open({message: '&nbsp' + vuex.state.errors.errors + '&nbsp', type: 'is-danger'})
		}

	}
}
