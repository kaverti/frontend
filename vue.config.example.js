const Dotenv = require('dotenv-webpack');
var WebpackAutoInject = require('webpack-auto-inject-version');
const fs = require("fs");

module.exports = {
 devServer: {
  proxy: 'http://localhost:23981',
  //disableHostCheck: true, // dev.kaverti.flowinity
  //host: 'dev.kaverti.flowinity',
  /*
  https: {
  cert: fs.readFileSync('C:/ssl/troplo/certificate.crt'),
  key: fs.readFileSync('C:/ssl/troplo/private.key'),
  }
  */
 },

 publicPath: '/',
 productionSourceMap: false,

 configureWebpack: {
  plugins: [
   new Dotenv(),
   new WebpackAutoInject({
    // specify the name of the tag in the outputed files eg
    // bundle.js: [SHORT] Version: 0.13.36 ...
    SHORT: 'Kaverti AIV',
    SILENT: false,
    PACKAGE_JSON_PATH: './package.json',
    PACKAGE_JSON_INDENT: 4,
    components: {
     AutoIncreaseVersion: true,
     InjectAsComment: true,
     InjectByTag: true
    },
    componentsOptions: {
     AutoIncreaseVersion: {
      runInWatchMode: true // it will increase version with every single build!
     },
     InjectAsComment: {
      tag: 'Kaverti Version: {version}, Build Date: {date}',
      dateFormat: 'dd/mm/yyyy; hh:MM:ss TT', // change timezone: `UTC:h:MM:ss` or `GMT:h:MM:ss`
      multiLineCommentType: false, // use `/** */` instead of `//` as comment block
     },
     InjectByTag: {
      fileRegex: /\.+/,
      AIVTagRegexp: /(\[AIV])(([a-zA-Z{} ,:;!()_@\-"'\\\/])+)(\[\/AIV])/g,
      dateFormat: 'dd/mm/yyyy; hh:MM:ss TT'
     }
    },
    LOGS_TEXT: {
     AIS_START: 'Kaverti AIV started'
    }
   })
  ]
 },

 pluginOptions: {
  i18n: {
   locale: 'en',
   fallbackLocale: 'debug',
   localeDir: 'locales',
   enableInSFC: false
  }
 }
}